<?xml version="1.0"?>
<xsl:stylesheet xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="m" version="1.0">

<!--
	 *
	 * Copyright (c) 2005+ MedHand International AB
	 * All rights reserved.
	 *
	 ***************************************************************
	 *
	 * @(#)wfml_to_mxml.xsl
   *
   * DESCRIPTION:
   * ======================
   * Stylesheet for converting  wfml XML to MedHand XML(MXML). 
   *
   * CHANGES:
   * ======================
	 -->


<!-- PRE-DEFINED FUNCTION TEMPLATES -->

<xsl:template name="getPreviousAttributes">
	<xsl:choose>
		<xsl:when test="@*">
			<xsl:for-each select="@*">
				<xsl:text> </xsl:text><xsl:value-of select="name()"/>=<xsl:text>"</xsl:text><xsl:value-of select="."/><xsl:text>"</xsl:text>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text></xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="*" mode="serialise">
  <xsl:text>&lt;</xsl:text>
  <xsl:value-of select="name()" />
  <xsl:for-each select="@*">
    <xsl:text> </xsl:text>
    <xsl:value-of select="name()" />
    <xsl:text>="</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>"</xsl:text>
  </xsl:for-each>
  <xsl:text>></xsl:text>
  <xsl:apply-templates select="node()" mode="serialise" />
  <xsl:text>&lt;/</xsl:text>
  <xsl:value-of select="name()" />
  <xsl:text>></xsl:text>
</xsl:template>

<!-- PRE-DEFINED FUNCTION TEMPLATES *END*-->

<!-- variable flag for moving misplaced process instructions or not -->
<xsl:variable name="move_pi" select="'1'"/>

<!-- book id variable -->
<xsl:variable name="book-id"><xsl:value-of select="/book/@id"/></xsl:variable>

<!-- function returning box background color -->
<xsl:template name="getBoxBackgroundColor">
	<xsl:param name="chapter-id"/>
	<xsl:attribute name="class">
		<xsl:text>outline-box-background-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning section title color -->
<xsl:template name="getSectionTitleColor">
	<xsl:attribute name="class">
		<xsl:text>section-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning part title color -->
<xsl:template name="getPartTitleColor">
	<xsl:attribute name="class">
		<xsl:text>part-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning chapter title color -->
<xsl:template name="getChapterTitleColor">
	<xsl:attribute name="class">
		<xsl:text>chapter-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div1 title color -->
<xsl:template name="getDiv1TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div1-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div2 title color -->
<xsl:template name="getDiv2TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div2-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div3 title color -->
<xsl:template name="getDiv3TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div3-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div4 title color -->
<xsl:template name="getDiv4TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div4-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div5 title color -->
<xsl:template name="getDiv5TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div5-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning text formatting -->
<xsl:template name="getFormatedText">
	<em>
		<xsl:attribute name="class">
				<xsl:text>text-foreground-color</xsl:text>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="ancestor::note"> 
				<b>
					<xsl:apply-templates/>
				</b>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</em>
</xsl:template>

<!-- MANIFEST ELEMENTS TEMPLATES -->
<xsl:variable name="document-id"><xsl:value-of select="/descendant::html/@id"/></xsl:variable>

<xsl:template match="manifest">
	<book pres-type="hard">
		<title>
			<xsl:value-of select="@title"/>
		</title>
		<img type="frontpage" src="frontpage.bmp"/> 
		<!-- PARAMETERS FOR LINEAR READING -->     
        <lr lrTop="1" lrBottom="1"/>
		<!-- PARAMETERS FOR LINEAR READING *END* --> 
		
		<xsl:apply-templates/>
	</book>
</xsl:template>

<xsl:template match="manifest-file">
	<xsl:variable name="title" select="@title"/>
	<xsl:choose>
		<xsl:when test="@title and preceding-sibling::manifest-file[@title = $title]">
		</xsl:when>
		<xsl:when test="@title">			
			<container pres-type="hard" p-id="{@id}">
				<title> 
					<xsl:attribute name="class">
						<xsl:value-of select="@color"/>
					</xsl:attribute>
					<xsl:value-of select="@title"/>		
				</title>
				<xsl:apply-templates select="document(@src)"/>
				
				<xsl:if test="following-sibling::manifest-file[@title = $title]">
					<xsl:for-each select="following-sibling::manifest-file[@title = $title]">
						<xsl:apply-templates select="document(./@src)"/>
					</xsl:for-each>
				</xsl:if>	
					
			</container>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="document(@src)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="part">

	<container pres-type="hard" p-id="{@id}">
		<title> 
			
		<xsl:value-of select="@title"/>		
		</title>
		<!-- <xsl:apply-templates select="document(@src)"/> -->
		<xsl:apply-templates select= "child::manifest-file"/>
			
		
	</container> 	

</xsl:template>

<!-- USER-DEFINED ELEMENTS TEMPLATES -->

<!-- root node, start building book -->

<xsl:template match="component" >
	 <xsl:apply-templates select="child::header|child::body"/>
</xsl:template>



<xsl:template match="blockFixed">
	<xsl:variable name="g-id" select="generate-id(.)"/>
	<container pres-type="none" p-id="{$g-id}">		
			
			<xsl:attribute name="class">inline-box-background-color</xsl:attribute> 
				<xsl:apply-templates/>
		</container>
</xsl:template>	 

<xsl:template match="a">
	<xref type="www" ref="{@href}">
		<xsl:apply-templates/>
	</xref>
</xsl:template>

<xsl:template match="verbatim">
   	
 		<xsl:apply-templates select="child::line"/>
  
</xsl:template>

<xsl:template match="line">
  <xsl:choose>
			<xsl:when test="@indentLevel='1'">
				<p indent="right">
					<xsl:apply-templates />
				</p>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:apply-templates />
				</p>
			</xsl:otherwise>
	</xsl:choose>
</xsl:template>	

<xsl:template match="header">
			<xsl:apply-templates select = "child::contentMeta"/>
</xsl:template>	

<xsl:template match="publicationMeta">
			<xsl:apply-templates/> 
</xsl:template>

<xsl:template match="contentMeta">
  
	<xsl:apply-templates select="child::creators|affiliationGroup"/>
	
</xsl:template>


<xsl:template match="creators">
	<p><b>
	<xsl:for-each select = "child::creator">

		<xsl:apply-templates select="child::personName/givenNames" />
		<xsl:text> </xsl:text>
		<xsl:apply-templates select="child::personName/familyName"/>
		
		<!-- Logic to put superscript for right affiliation   -->
		<xsl:if test= "count(ancestor::contentMeta/affiliationGroup/affiliation)>1"> 
			<sup><xsl:value-of select="number(substring(@affiliationRef,string-length(@affiliationRef)-1))"/> </sup> 
		</xsl:if>
		
		<!-- Logic to put , or and or none depending on how many names are remaining -->
		<xsl:if test="count(following-sibling::creator) = 1"> and </xsl:if>
		<xsl:if test="count(following-sibling::creator) > 1">, </xsl:if>	
	</xsl:for-each>
	</b></p>
	
</xsl:template>

<xsl:template match="affiliationGroup">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="affiliation">
	<p>
	<!-- Logic to display the superscript for university  -->
	<xsl:if test= "count(following-sibling::affiliation)>0 or count(preceding-sibling::affiliation)>0">
	<sup><xsl:value-of select="number(substring(@xml:id,string-length(@xml:id)-1))"/> </sup></xsl:if>
	
	<xsl:if test = "child::orgDiv">
		<xsl:value-of select="orgDiv"/>,
	</xsl:if>
	<xsl:value-of select="orgName"/>
	<xsl:if test="address/city">
		, <xsl:value-of select="address/city"/>
	</xsl:if>
	<xsl:if test="address/city">
		, <xsl:value-of select="address/country"/>
	</xsl:if>
	</p>
</xsl:template>

<xsl:template match="body">

			<xsl:apply-templates select="child::p|child::section|child::bibliography|child::feature|child::noteGroup"/>

</xsl:template>

<xsl:template match="br">
	<p></p>
</xsl:template>

<xsl:template match="section">
<xsl:variable name="chapter-name">
	<xsl:value-of select="concat(concat(ancestor::component/@id,'.html#'),@id)"/>
</xsl:variable>

	<xsl:choose>
		<xsl:when test = " contains(@xml:id,'sec1-0001')">
		    <container pres-type="none">	
					<xsl:apply-templates/>
			</container>
		</xsl:when>
		
		<!-- only for contributors to appear in a table -->		
		<xsl:when test="contains(child::title,'Contributors')">
			<container pres-type="hard"  p-id="{@xml:id}" >
				<xsl:apply-templates/>
				<!-- <table border="false">
					<xsl:for-each select="child::p[number(substring-after(@xml:id,'f01-lst1-00')) &lt; 8]">
						<xsl:variable name="thisCol" select="@xml:id"/>
						<xsl:variable name="nextCol">
							<xsl:variable name="no"><xsl:value-of select="number(substring-after(@xml:id,'f5-para-00'))+7"/> </xsl:variable>
							<xsl:if test="$no &lt; 10">
								<xsl:value-of select="concat('f5-para-000',$no)"/>
							</xsl:if>
							<xsl:if test="$no &gt; 9">
								<xsl:value-of select="concat('f5-para-00',$no)"/>
							</xsl:if>
						</xsl:variable>
						
						<tr>
							<td>
								<xsl:apply-templates select="parent::section/p"/>
							</td>
							<td> [contains(@xml:id,$thisCol)]
								<xsl:apply-templates select="parent::section/p[contains(@xml:id,$nextCol)]"/>
							</td>
						</tr>
					</xsl:for-each>
					<tr>
						
					</tr>
				</table> -->
			</container>
		</xsl:when>
		<xsl:when test= "contains(@xml:id,'f')">
			<container pres-type="hard" p-id="{@xml:id}" >
				<xsl:apply-templates/>
			</container>
		</xsl:when>
		<xsl:when test= "parent::body">
			<container pres-type="hard" p-id="{@xml:id}">
<!-- 				<xsl:if test="contains(@type,'summary')">
					<xsl:attribute name="class">
						<xsl:value-of select="concat('outline-box-background-',ancestor::body/@color)"/>
					</xsl:attribute>
				</xsl:if> -->
				<xsl:apply-templates/>
		 	</container>
		</xsl:when>
				
		<xsl:otherwise>
			 <container pres-type="none">
			 	<xsl:apply-templates/>
			</container>
		</xsl:otherwise>
	</xsl:choose>
 
</xsl:template>

<xsl:template match="otherTitle">
<xsl:apply-templates/>
</xsl:template>	
<xsl:template match="chapterTitle">
<xsl:apply-templates/>
</xsl:template>	

<xsl:template match="closing ">
	<xsl:apply-templates/>
</xsl:template>
	
<xsl:template match="noFigureGroup">
	<xsl:apply-templates/>
</xsl:template>	

<!--<xsl:template match="blockFixed">
	<container pres-type="none" p-id="{@xml:id}">
			<img type="normal" src="{@xml:id}"/>
	</container>
</xsl:template>	-->

<xsl:template match="symbol">
	<symbol type="image" src="{@src}"/>
</xsl:template>
<xsl:template match="img">
	<p><symbol type="image" src="{@src}"/></p>
</xsl:template>

<xsl:template match="figure">
	<xsl:variable name="Chap">
		<xsl:value-of select="number(substring-before(substring-after(@xml:id,'manion6298c'),'-fig-'))"/>
	</xsl:variable>
	<xsl:variable name="img">
		<xsl:value-of select="substring-after(@xml:id,'manion6298')"/>
	</xsl:variable>
	<xsl:variable name="figno">
		<xsl:value-of select="number(substring-after(substring-after(@xml:id,'manion6298'),'-fig-'))"/>
	</xsl:variable>
	<container pres-type="none" p-id="{@xml:id}">
		<p> <b>Figure <xsl:value-of select="$Chap"/>.<xsl:value-of select="$figno"/> <xsl:text> </xsl:text> 
			<keyword search-class="image">	<xsl:apply-templates select="child::caption/p"/></keyword><xsl:text> </xsl:text></b>
		</p>
		<img type="normal" src="{$img}"/>
		<p>	<xsl:apply-templates select="child::caption/source"/>
		</p>	
		
	</container>
				
</xsl:template>
<xsl:template match="link">
	
	
		<xsl:choose>
			<xsl:when test="contains(@href,'http://') or contains(@href,'https://') or contains(@href,'www')">
				<xref type="www" ref="{@href}">
					<xsl:apply-templates/>
				</xref>
			</xsl:when>
			<xsl:when test="contains(@href,'-fig-')">
				<xsl:variable name="chapter">
					<xsl:value-of select="number(substring-before(substring-after(@href,'manion6298c'),'-fig-'))"/> 
					
				</xsl:variable>
				<xsl:variable name="figno">
					<xsl:value-of select="number(substring-after(@href,'fig-'))"></xsl:value-of>
				</xsl:variable>
				<xsl:variable name="target">
					<xsl:choose>
						<xsl:when test="contains(@href,'manion6298p')">
							<xsl:value-of select="substring-after(@href,'#')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring-after(@href,'#')"/>
						</xsl:otherwise>
					</xsl:choose>
					
				</xsl:variable>				
				<xref type="page" ref="{$target}">
					<xsl:call-template name="getLinkText">
						<xsl:with-param name="type" select="concat('Figure ',concat(concat($chapter,'.'),$figno))"/>
					</xsl:call-template>
					<xsl:value-of select="xref"/>
				</xref>
			</xsl:when>
			<xsl:when test="contains(@href,'-tbl-')">
				<xsl:variable name="chapter">
					<xsl:value-of select="number(substring-before(substring-after(@href,'manion6298c'),'-tbl-'))"/>
				</xsl:variable>
				<xsl:variable name="tblno">
					<xsl:value-of select="number(substring-after(@href,'-tbl-'))"></xsl:value-of>
				</xsl:variable>
				<xsl:variable name="target">
					<xsl:value-of select="substring-after(@href,'#')"/>
				</xsl:variable>				
				<xref type="page" ref="{$target}">
					<xsl:call-template name="getLinkText">
						<xsl:with-param name="type" select="concat('Table ',concat(concat($chapter,'.'),$tblno))"/>
					</xsl:call-template>
					<xsl:value-of select="xref"/>
				</xref>

			</xsl:when>
			<xsl:when test="contains(@href,'-sec')">
				<xsl:variable name="chap">
					<xsl:value-of select="number(substring-before(substring-after(@href,'manion6298c'),'-sec'))"/>
				</xsl:variable>
				<xsl:variable name="secno">
					<xsl:value-of select="number(substring-before(substring-after(@href,'-sec'),'-'))"></xsl:value-of>
				</xsl:variable>
				<xsl:variable name="subsec">
					<xsl:value-of select="number(substring-after(@href,'-000'))"></xsl:value-of>
				</xsl:variable>
				<xsl:variable name="target">
					<xsl:value-of select="substring-after(@href,'#')"/>
				</xsl:variable>				
				<xref type="page" ref="{$target}">
					<xsl:call-template name="getLinkText">
						<xsl:with-param name="type" select="concat('Section ',concat(concat(concat($chap,'.'),$secno),'.'),$subsec)"/>
					</xsl:call-template>
					<xsl:value-of select="xref"/>
				</xref>
			
			</xsl:when>
			<xsl:when test="contains(@href,'-bib-')">
				
					<xsl:choose>
						<xsl:when test="contains(@href,'manion6298')">
							<xsl:variable name="bib">
								<xsl:value-of select="substring-after(@href,'#')"/> 
							</xsl:variable>
							<xsl:variable name="bibTxt">
								<xsl:choose>
								<xsl:when test= "contains(@href,'f')">
									<xsl:value-of select="ancestor::body/bibliography/bib[contains($bib,@xml:id)]/citation/pubYear"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="ancestor::body/bibliography/bib[contains($bib,@xml:id)]/citation/pubYear"/>
								</xsl:otherwise>
								
								</xsl:choose>
								
							</xsl:variable> 
							
							<fref ref="{$bib}" symbol="{$bibTxt}">
								
							</fref>
						    <footnote p-id="{$bib}">
						    	<p><xsl:value-of select="ancestor::body/bibliography/bib[contains($bib,@xml:id)]"/></p>
						    </footnote>
							
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="bib">
								<xsl:value-of select="substring-after(substring-after(@href,'manion6298'),':')"/>
							</xsl:variable>
							<xsl:variable name="bibTxt">
								<xsl:value-of select="ancestor::body/bibliography/bibSection/bib[contains($bib,@xml:id)]/citation/pubYear"/>
							</xsl:variable> 
							
							<fref ref="{$bib}" symbol="{$bibTxt}">
								<xsl:if test="parent::title">
									<xsl:attribute name="class">
										<xsl:value-of select="concat('chapter-foreground-',ancestor::body/@color)"/>
									</xsl:attribute>
								</xsl:if>
							</fref>
						    <footnote p-id="{$bib}">
						    	<p><xsl:value-of select="ancestor::body/bibliography/bibSection/bib[contains($bib,@xml:id)]"/></p>
						    </footnote>
						</xsl:otherwise>
					</xsl:choose>
			
			</xsl:when>
			<xsl:when test="contains(@href,'-note-')">
				<xsl:variable name="sym">
					<xsl:value-of select="substring-after(substring-after(@href,'-note-'),'-')"/>
				</xsl:variable>
				<xsl:variable name="target">
					<xsl:value-of select="substring-after(@href,'#')"/>
				</xsl:variable>				
				<!-- <xref type="page" ref="{$target}">
					<xsl:call-template name="getLinkText">
						<xsl:with-param name="type" select="concat('[',concat($sym,']'))"/>
					</xsl:call-template>
					<xsl:value-of select="xref"/>
				</xref> -->
				<fref ref="{$target}" symbol="{$sym}">
				</fref>
				 <footnote p-id="{$target}">
				 <xsl:if test= "ancestor::tabular">
				 	<p><xsl:value-of select="ancestor::tabular/noteGroup/note[@xml:id=$target]"/></p>
				</xsl:if>
				<xsl:if test= "ancestor::section">
				 	<p><xsl:value-of select="ancestor::section/noteGroup/note[@xml:id=$target]"/></p>
				</xsl:if>
				 </footnote>

			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="chapId">
						<xsl:value-of select="substring-after(@href,'component:9780470886298')"/>
				</xsl:variable>
				<xref type="page" ref="{$chapId}">
					<xsl:call-template name="getLinkText">
						<xsl:with-param name="type" select="concat('Chapter ',number(substring-after($chapId,'c')))"/>
					</xsl:call-template>
					<xsl:value-of select="xref"/>
				</xref>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template match="endNotes">
	<xsl:apply-templates/>
</xsl:template>
<!-- feature container -->


<xsl:template match="abbrev">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="tabular">

	<xsl:variable name="tablecount">
		<xsl:choose>
			<xsl:when test="number(substring(@xml:id,string-length(@xml:id)-1))">
				<xsl:value-of select="number(substring(@xml:id,string-length(@xml:id)-1))"/>			
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring(@xml:id,string-length(@xml:id)-1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<container pres-type="none" p-id="{@xml:id}"> <!-- class="{concat('outline-box-background-',ancestor::body/@color)}" -->
	<xsl:choose>
			<xsl:when test= "child::title">
				<p>
				<b>Table <xsl:value-of select="number(substring-after(substring-before(@xml:id,'-'),'c'))"/>.<xsl:value-of select="$tablecount"/> <xsl:text> </xsl:text>
				<keyword search-class="table"><xsl:apply-templates select="child::title"/>
				</keyword>
				</b></p>
			</xsl:when>
			<xsl:when test="child::titleGroup">
				<title class="title-color" search-class="table">
				<b>Table <xsl:value-of select="number(substring-after(substring-before(@xml:id,'-'),'c'))"/>.<xsl:value-of select="$tablecount"/></b> <xsl:text> </xsl:text>
				<xsl:value-of select="child::titleGroup/title[2]"/>
				</title>
			</xsl:when>
	</xsl:choose>
			
	<!-- table -->
	<table border="true">
		<xsl:apply-templates select="child::table|child::source|child::noteGroup"/>
	</table>
</container>
</xsl:template>

<xsl:template match="tabularFixed">
<xsl:variable name="g-id" select="generate-id(.)"/>
	<container pres-type="none" p-id="{$g-id}">		
	
	<!-- table -->
	<table border="false">
		
		<xsl:apply-templates select="child::table|child::source|child::noteGroup"/>
		
	</table>
	</container>
	
</xsl:template>

<xsl:template match="table">

	<xsl:apply-templates select="child::tgroup"/>

</xsl:template>

<xsl:template match="tgroup">
	<xsl:if test="child::thead">
		<xsl:for-each select="child::thead/row">
			<tr>
			<xsl:for-each select="child::entry">
				<xsl:variable name = "colspanno">
					<xsl:choose>
						<xsl:when test="@namest and @nameend"><xsl:value-of select="1+number(substring-after(@nameend,'col'))-number(substring-after(@namest,'col'))"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
				<xsl:when test="@nameend">
					<td colspan="{$colspanno}" rowspan="{number(@morerows)+1}"><b><xsl:value-of select="."/></b></td>
				</xsl:when>
				<xsl:when test="child::p[2]/url">
					<td rowspan="{number(@morerows)+1}"><b><xsl:apply-templates select="child::p[1]"/></b>
					<xsl:if test="child::p[2]">
						<p></p>
						<xsl:apply-templates select="child::p[2]"/>
					</xsl:if>					
					</td>
				</xsl:when>								
				<xsl:otherwise>
					<td rowspan="{number(@morerows)+1}"><b><xsl:apply-templates /></b></td>
				</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</tr>
		</xsl:for-each>
	</xsl:if>		
	<xsl:for-each select="child::tbody/row">
		<tr>
		<xsl:for-each select="entry">
			<xsl:choose>
			<xsl:when test="descendant::list1">
				<td><xsl:apply-templates/></td>
			</xsl:when>
	
			<xsl:when test="@indentLevel">
				<xsl:variable name="indentValue"> <xsl:value-of select="number(@indentLevel)*20"/> </xsl:variable>
				<td><p indent="{$indentValue}"><xsl:apply-templates/></p></td>
			</xsl:when>
			
			<xsl:otherwise>
				<td rowspan="{number(@morerows)+1}"><xsl:apply-templates/></td>
			</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		</tr>
		</xsl:for-each>	   

</xsl:template>

<xsl:template match="title">
	
	 
	<xsl:choose>
		<xsl:when test="parent::section">
				
			<title>
				<xsl:if test="not(contains(ancestor::component/@xml:id,'9780470886298f01'))">
					<xsl:attribute name="search-class">title</xsl:attribute>
				</xsl:if> 
					
				<xsl:choose>
					<xsl:when test="contains(ancestor::component/@xml:id,'9780470886298f')">
					
						<xsl:apply-templates/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates/>
					</xsl:otherwise>	
				</xsl:choose>
		 	</title>
		</xsl:when>
		
		<xsl:when test="parent::sect1|parent::bibSection">
			<title  class="{concat('chapter-foreground-',ancestor::body/@color)}"> 
				<xsl:apply-templates/>
			 </title>
		</xsl:when>	
			 
		<xsl:when test="parent::tabularFixed">
			<b><xsl:value-of select="normalize-space(.)"/></b>
		</xsl:when>		
			
		<xsl:when test="parent::listItem">
			<i><xsl:value-of select="normalize-space(.)"/></i>
		</xsl:when>		 
			
		<xsl:when test="parent::tabular">
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:when>		
		<xsl:when test="child::fi">
				<xsl:apply-templates/>
		</xsl:when>
		
		<xsl:when test="parent::listItem | parent::item1">
				<b><xsl:value-of select="normalize-space(.)"/></b>
		</xsl:when>	

		<xsl:when test="parent::listPaired">
			<title> 
				<xsl:variable name="listTitleColor">
					<xsl:call-template name="getTitleColor">
	      				<xsl:with-param name="title" select = "." />
	    			</xsl:call-template>
	    		</xsl:variable>
	    		<xsl:attribute name="class"><xsl:value-of select="$listTitleColor"/></xsl:attribute>	
				<xsl:apply-templates/>
			 </title>
		</xsl:when>	
	
		<xsl:otherwise>
			<title class="{concat('chapter-foreground-',ancestor::body/@color)}">
				<xsl:if test="not(contains(ancestor::component/@xml:id,'w9781118515778f1'))">
					<xsl:attribute name="search-class">title</xsl:attribute>
				</xsl:if>
				<xsl:apply-templates/>
			 </title>
		</xsl:otherwise>
	</xsl:choose>
	 
</xsl:template>

<xsl:template match="fi">
	<i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="p">

	<xsl:choose>
	<xsl:when test="parent::entry/@indentLevel">
		<xsl:variable name="indentValue"> <xsl:value-of select="number(parent::entry/@indentLevel)*20"/> </xsl:variable>
		<p indent="{$indentValue}">
		 	<xsl:apply-templates/>
		</p>
	</xsl:when>
	<xsl:when test="parent::item1 | parent::listItem | parent::blockFixed[parent::listItem] | parent::p | parent::entry">
		<xsl:apply-templates/>
	</xsl:when>	
<!-- 	<xsl:when test="parent::caption and not(child::list1)">
		<keyword search-class="image"><xsl:apply-templates/></keyword> 
	</xsl:when> -->
		<xsl:when test="parent::blockFixed">
		<p><i><xsl:apply-templates/></i></p>
	</xsl:when>
	<xsl:when test="parent::caption">
		<xsl:apply-templates/>
	</xsl:when>
	<xsl:when test="child::blockFixed">
		<p><i><xsl:value-of select="text()"/></i></p>
		<xsl:apply-templates select= "child::blockFixed"/>
	</xsl:when>
	<xsl:when test="child::inlineGraphic">
		<xsl:apply-templates select= "child::inlineGraphic"/>
	</xsl:when>
	<xsl:when test="child::featureFixed">
		<p><xsl:value-of select="text()"/></p>
		<xsl:apply-templates select= "child::featureFixed"/>
	</xsl:when>
	<xsl:otherwise>
		<p>
		 	<xsl:apply-templates/>
		</p>
	</xsl:otherwise>
	</xsl:choose>
		
</xsl:template>
<xsl:template match="definitionList">
	<container pres-type="none">
		<xsl:apply-templates/> 
	</container>	
</xsl:template>



<xsl:template match=" list">

	
	<list>
	<!-- set list type -->
	<xsl:attribute name="type">
		<xsl:choose>
			<xsl:when test="starts-with(@style,'A')">
					<xsl:text>ALFA</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(@style,'a')">
					<xsl:text>alfa</xsl:text>
			</xsl:when>
			<xsl:when test="contains(@style,'square')">
					<xsl:text>square</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(@style,'1')">
					<xsl:text>number</xsl:text>
			</xsl:when>
			<xsl:when test="contains(@style,'plain')">
					<xsl:text>plain</xsl:text>
			</xsl:when>
			<xsl:otherwise>
					<xsl:text>>bullet</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:attribute>
	
	<xsl:apply-templates/>
	</list>
	

</xsl:template>

<xsl:template match="listItem">
	
		<xsl:choose>
			<xsl:when test="parent::listItemPair">
				<p></p><xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<item>
				<!-- item content -->
					<p><xsl:apply-templates/></p>
				</item>							
			</xsl:otherwise>
		</xsl:choose>
	
</xsl:template>
		

<!-- figure(image or symbol) container -->


<xsl:template match="inlineGraphic">
	<xsl:variable name="img">
		<xsl:value-of select="substring-after(substring-after(@href,'media:'),':')"/>
	</xsl:variable>
	<container pres-type="none">
		<p><symbol type="image" src="{$img}"/></p>
	</container>
</xsl:template>
	
<!-- normal caption -->
<xsl:template match="caption">
		
		<xsl:choose>
			<xsl:when test="child::p or parent::figure">
				<xsl:apply-templates/> 
			</xsl:when>
			<xsl:otherwise>
				<p><xsl:apply-templates/></p>
			</xsl:otherwise>
		</xsl:choose>
			
	
</xsl:template>
		
<!-- normal source -->
<xsl:template match="source">
	<xsl:choose>
		<xsl:when test= "parent::blockQuote | parent::blockFixed | parent::feature">
		<p><xsl:apply-templates/></p>
		</xsl:when>
		<xsl:when test="parent::tabular|tabularFixed">
			<tr>
				<td colspan="10"><xsl:apply-templates/></td>
			</tr>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>		

<!-- Images *end* -->
<xsl:template name="getLinkText">
	<xsl:param name="type"/>
	<xsl:choose>
		<!-- bold linkt text? -->
		<xsl:when test="child::b">
			<b>
				<!-- shoud there be a 'see' included in the link text or not -->
				<xsl:if test="not(contains(ancestor::p, '&#x2013;') or contains(ancestor::p, 'See') or contains(ancestor::p, 'see') or contains(ancestor::p/parent::node(), 'See') or contains(ancestor::p/parent::node(), 'see') or contains(parent::node(), 'See') or contains(parent::node(), 'see'))">
					<xsl:text></xsl:text>
				</xsl:if>
				<xsl:value-of select="$type"/>
			</b>
		</xsl:when>
		<xsl:otherwise>
			<!-- shoud there be a 'see' included in the link text or not -->
			<xsl:if test="not(contains(ancestor::p, '&#x2013;') or contains(ancestor::p, 'See') or contains(ancestor::p, 'see') or contains(ancestor::p/parent::node(), 'See') or contains(ancestor::p/parent::node(), 'see') or contains(parent::node(), 'See') or contains(parent::node(), 'see'))">
				<xsl:text></xsl:text>
			</xsl:if>
			<xsl:value-of select="$type"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- external link -->
<xsl:template match="url">
	
		<!-- url variable -->
		<xsl:variable name="url">
			<xsl:choose> 
				<xsl:when test="@href" >
					<xsl:value-of select="@href"/>
				</xsl:when>			
				<xsl:when test="@webUrl">
					<xsl:value-of select="@webUrl"/>
				</xsl:when>
				<xsl:when test="@canonical">
					<xsl:value-of select="@canonical"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<!-- external xref -->
		<xref type="www" ref="{$url}">
			<xsl:apply-templates/>
		</xref>
	
</xsl:template>

<!-- bibliography section -->


<xsl:template match="bibliography">

 	<xsl:if test= "contains(@xml:id,'b01')">
 	
				<xsl:apply-templates select ="child::bib"/>

 </xsl:if>
	
</xsl:template>

<xsl:template match="bibSection">
	<xsl:choose>
		<xsl:when test= "child::title">
			<container pres-type="hard" p-id="{@xml:id}">
				<xsl:apply-templates />
		</container>	
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="bib">
		<container pres-type="none">
			<p><xsl:apply-templates/></p>
		</container>
</xsl:template>
<!-- bibliography item -->


<xsl:template match="citation">
			<xsl:apply-templates/>
</xsl:template>


<xsl:template match="journalCit">
	<xsl:apply-templates/>
</xsl:template>
<xsl:template match="articleTitle"> 
	<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="journalTitle"> 
	<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="groupName">
	<xsl:apply-templates/>
</xsl:template>
<xsl:template match="author">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="givenNames"> 
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="surname"> 
	<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="familyName"> 
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="familyNamePrefix"> 
	<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="forenames"> 
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="bookTitle"> 
	<i><xsl:value-of select="."/></i>
</xsl:template>
<xsl:template match="publisherName"> 
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="publisherLoc"> 
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="fc">
	<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="email">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="pubYear">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="vol">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="accessionId">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="issue">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="suppInfo">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="pageFirst">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="pageLast">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="corpAuthor">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="plaintiff">
	<xsl:apply-templates/>
</xsl:template>


<xsl:template match="foreignPhrase">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="defendant">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match=" span">
	<xsl:apply-templates/>
</xsl:template>


<xsl:template match="mtext">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="mfrac">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="mfenced">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="msup">
	<xsl:apply-templates/>
</xsl:template>

<!-- FORMATING -->

<!-- bold italic -->	
<xsl:template match="bi">
	
		<b>
			<i>
				<xsl:apply-templates/>
			</i>
		</b>
	
</xsl:template>

<!-- bold -->	
<xsl:template match="b">
	
		<xsl:choose>
			<xsl:when test="parent::questionPlain">
				<b> 
					<xsl:apply-templates/> 
				</b>
			</xsl:when>
			<xsl:otherwise>
				<b> <xsl:apply-templates/> </b>
			</xsl:otherwise>
		</xsl:choose>
		
	
</xsl:template>

<!-- italic -->	
<xsl:template match="i">
	
		<i>
			<xsl:apply-templates/>
		</i>
	
</xsl:template>

<xsl:template match="sc">
	
			<xsl:apply-templates/>
	
</xsl:template>
<!-- subscript -->	
<xsl:template match="sub">
	
		<sub>
			<xsl:apply-templates/>
		</sub>
	
</xsl:template>
<!-- superscript -->	
<xsl:template match="sup">
	
		<sup>
			<xsl:apply-templates/>
		</sup>
	
</xsl:template>
<!-- FOOTNOTE *end* -->
<xsl:template match="label">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mathDisplay">
	
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="mrow">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mtable">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mtd">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mtr">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mi">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mn">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mo">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="msubsup">
	
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="msub">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="mspace">
	
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="featureFixed">
<xsl:variable name="g-id" select="generate-id(.)"/>			
	<xsl:choose>
	 <xsl:when test= "contains(child::title/@type,'featureFixedName')">		
		<container pres-type="none" p-id="{$g-id}">
			<xsl:attribute name="class">inline-box-background-color</xsl:attribute>
				<xsl:apply-templates select ="child::p" />
		</container>
	</xsl:when>
	<xsl:when test= "contains(child::titleGroup/title[2]/@type,'main')">
		
	
	</xsl:when>	
	</xsl:choose>
</xsl:template>	

<xsl:template match="feature">
	<xsl:variable name="g-id" select="generate-id(.)"/>	
	<container pres-type="none" p-id="{$g-id}">
			<xsl:attribute name="class">spline-box-background-color</xsl:attribute>
				<xsl:apply-templates select= "child::titleGroup/title[2]|child::section"/>
		</container>
</xsl:template>

<xsl:template match="termDefinition">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="term">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="bookCit">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="editor">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="edition">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="item2">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="nameSuffix">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="surnamePrefix">
		
		<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="noTitle">
		
	
</xsl:template>

<xsl:template match="mathProblem">
		
	<i>Example <xsl:value-of select="number(substring(@id,string-length(@id)-1))"></xsl:value-of></i>
		<xsl:value-of select="."/>
		
</xsl:template>

<xsl:template match="abbrevListGenerated">
		
	
</xsl:template>
<xsl:template match="alternativeText">
		
		
	
</xsl:template>	
<xsl:template match="titleGroup">
		
		<xsl:choose>
		<xsl:when test = "contains('child::title/@type','featureName')">
		 	<xsl:apply-templates select = "child::title"/>
		</xsl:when>
		<xsl:when test="child::title[1]/link">
			<xsl:apply-templates select="child::title[1]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates  select = "child::title[1]"/>
		</xsl:otherwise>
		</xsl:choose>
	
</xsl:template>	

<xsl:template match="numberingGroup">
		
		
	
</xsl:template>

<xsl:template match="displayedItem">

	<xsl:apply-templates/>
	
</xsl:template>

<xsl:template match="math">

	 <xsl:variable name="symb">
	 	<xsl:value-of select="substring-after(substring-after(@altimg,'media:'),':')"/>
	 </xsl:variable>
	 <br/>
	<symbol type="image" src="{$symb}"/>
	
</xsl:template>

<xsl:template match="lineatedText">
		
		<xsl:apply-templates/>
	
</xsl:template>	

<xsl:template match="mediaResource">
		
		<xsl:apply-templates/>
	
</xsl:template>
<xsl:template match="noteGroup">
		<xsl:apply-templates select="child::note"/>
</xsl:template>
<xsl:template match="note">
	
		<xsl:variable name="id">
			<xsl:value-of select="substring-after(substring-after(@xml:id,'-note-'),'-')"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="(ancestor::boxedMatter or ancestor::figureGroup) and not(ancestor::book/descendant::xref[@ref = $id])">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="(ancestor::tabular) or ancestor::section and (contains($id,'a') or contains($id,'b') or contains($id,'c') or contains($id,'1'))">
			<!-- <container pres-type="none" p-id="{@xml:id}"> -->
			<xsl:if test= "parent::Notegroup/title">
			<p><b><xsl:value-of select= "parent::Notegroup/title"/> </b></p>			
			</xsl:if>
			<p> <sup><xsl:value-of select="concat('[',concat($id,']'))"/> </sup><xsl:value-of select="child::p"/></p>
			<!-- </container> -->
			</xsl:when>
			<xsl:when test="(ancestor::tabular)">
			<!-- <container pres-type="none" p-id="{@xml:id}"> -->
			<p> <xsl:value-of select="child::p"/></p>
			<!-- </container> -->
			</xsl:when>
			
			<xsl:otherwise>
					<xsl:apply-templates/>
				<!-- <footnote p-id="{@xml:id}"></footnote> -->	
			</xsl:otherwise>
		</xsl:choose>
	
</xsl:template>
<xsl:template match="listItemPair">
		
		<p><xsl:apply-templates/></p>
	
</xsl:template>
<xsl:template match="listPaired">
		
		<xsl:apply-templates/>
	
</xsl:template>


<xsl:template match="exercises">
		
		<xsl:apply-templates/>
	
</xsl:template>


<xsl:template match="copyright">
		
		
	
</xsl:template>
<xsl:template match="publisherInfo">
		
		
	
</xsl:template>
<xsl:template match="isbn">
		
		
	
</xsl:template>
<xsl:template match="eventGroup">
		
		
	
</xsl:template>
<xsl:template match="idGroup">
		
		
	
</xsl:template>
<xsl:template match="subjectInfo">
		
		
	
</xsl:template>

<!-- Macro to identify Title Color -->
<xsl:template name="getTitleColor" >
	<xsl:param name="title"/>
	
	<xsl:variable name="purple"><xsl:text>Chapter 1 Chapter 9 Chapter 17</xsl:text></xsl:variable>
	<xsl:variable name="blue"><xsl:text>Chapter 2 Chapter 10</xsl:text></xsl:variable>
	<xsl:variable name="teal"><xsl:text></xsl:text>Chapter 3 Chapter 11 Chapter 18</xsl:variable>
	<xsl:variable name="green"><xsl:text>Chapter 4 Chapter 12</xsl:text></xsl:variable>
	<xsl:variable name="yellow"><xsl:text>Chapter 5 Chapter 13</xsl:text></xsl:variable>
	<xsl:variable name="red"><xsl:text>Chapter 6 Chapter 14</xsl:text></xsl:variable>
	<xsl:variable name="pink"><xsl:text>Chapter 7 Chapter 15</xsl:text></xsl:variable>
	<xsl:variable name="lavender"><xsl:text>Chapter 8 Chapter 16</xsl:text></xsl:variable>

	<xsl:choose>
		<xsl:when test="contains($purple,$title)">chapter-foreground-purple</xsl:when>
		<xsl:when test="contains($blue,$title)">chapter-foreground-blue</xsl:when>
		<xsl:when test="contains($teal,$title)">chapter-foreground-teal</xsl:when>
		<xsl:when test="contains($green,$title)">chapter-foreground-green</xsl:when>
		<xsl:when test="contains($yellow,$title)">chapter-foreground-yellow</xsl:when>
		<xsl:when test="contains($red,$title)">chapter-foreground-red</xsl:when>
		<xsl:when test="contains($pink,$title)">chapter-foreground-pink</xsl:when>
		<xsl:when test="contains($lavender,$title)">chapter-foreground-lavender</xsl:when>
	</xsl:choose>

</xsl:template>

<!-- Macro to identify feature titles & numbering -->
<xsl:template name="getFeatureTitle" >
	<xsl:param name="feature"/>
	
	<xsl:variable name="title">
		<xsl:value-of select="$feature/descendant-or-self::*/title"/>
	</xsl:variable>
	
	<xsl:variable name="chapter">
		<xsl:value-of select="number(substring-after(substring-before($feature/@xml:id,'-'),'c'))"/> 
	</xsl:variable>
	
	<xsl:variable name="xmlIds">
		<xsl:for-each select="/component/body/section/descendant-or-self::*/feature">
			<xsl:if test="contains(descendant-or-self::*/title,$title)">
				<xsl:value-of select="@xml:id"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
		
	<xsl:variable name="featId">
		<xsl:value-of select="(string-length(substring-before($xmlIds,$feature/@xml:id)) div string-length($feature/@xml:id)) + 1"/>
	</xsl:variable>	
	
	<xsl:value-of select="concat(concat($title,' '),concat(concat($chapter,'.'),$featId))"/>
	
</xsl:template>
        
<!-- USER-DEFINED ELEMENTS TEMPLATES *END*-->

<!-- UNDEFINED ELEMENT TEMPLATES -->

<!-- This is were a unndefined element ends up. -->
<xsl:template match="*">
	[%WARNING% message=No stylesheet entry for: <xsl:value-of select="name()"/>]
</xsl:template>

<!-- UNDEFINED ELEMENT TEMPLATES *END*-->

</xsl:stylesheet>
