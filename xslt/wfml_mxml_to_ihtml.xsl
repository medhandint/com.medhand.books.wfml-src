<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" xmlns:set="http://exslt.org/sets" exclude-result-prefixes="exsl set">

<!--
	/*************************************************************************
	 *
	 * Copyright (c) 2005+ MedHand International AB
	 * All rights reservered.
	 *
	 *************************************************************************/
	/*
	 * @(#)weie_mxml_to_ihtml.xsl
	 *
   * DESCRIPTION:
   * ====================
   * Stylesheet for converting MedHand XML(MXML) to iHtml.
   *
   * OVERRIDES:
   * ====================
   *
	 * CHANGES:
   * ====================
	 */-->
	 
<xsl:import href="../../common/ihtml/xslt/mxml_to_ihtml.xsl"/>
<xsl:variable name="style" select="'normal-style'"/>     
<xsl:variable name="folder-max-size" select="0"/>

<!-- *******************************************************************
			BOOK SPECIFIC FORMATING
     ******************************************************************* -->
<!-- *******************************************************************
			Breadcrumbs
     ******************************************************************* -->
      <!-- Title -->
<xsl:template name="getSearchClassBreadcrumb">
	<xsl:choose>
		<xsl:when test="ancestor::container[@pres-type='hard'][2]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][1]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/> 
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
		</xsl:otherwise>
		
	</xsl:choose>
</xsl:template>

<!-- Keyword -->
<xsl:template name="getSearchKeywordBreadcrumb">
	<xsl:choose>
		<xsl:when test="ancestor::container[@pres-type='hard'][2]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][1]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/> 
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Bookmark & History Description -->
<xsl:template name="getSectionBreadcrumb">
	<xsl:choose>
		<xsl:when test="ancestor::container[@pres-type='hard'][3]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][2]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[parent::container]/title)"/>
		</xsl:when>
		<xsl:when test="ancestor::container[@pres-type='hard'][2]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][2]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][2]/title)"/> 
		</xsl:when>
		<xsl:otherwise>	<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/> </xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Bookmark & History description for table -->
<xsl:template name="getTableBreadcrumb">
	<xsl:choose>
		<xsl:when test="ancestor::container[@pres-type='hard'][3]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][1]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[parent::container]/title)"/>
		</xsl:when>
		<xsl:when test="ancestor::container[@pres-type='hard'][2]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][1]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][2]/title)"/> 
		</xsl:when>
		<xsl:otherwise>	<xsl:value-of select="normalize-space(ancestor::container[parent::container]/title)"/> </xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Bookmark & History description for image -->
<!-- image breadcrumbs, used in html header and and describes enviroment for a image bookmark&history entry  -->  
<xsl:template name="getImageBreadcrumb">
	<xsl:choose>
		<xsl:when test="ancestor::container[@pres-type='hard'][3]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][1]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[parent::container]/title)"/>
		</xsl:when>
		<xsl:when test="ancestor::container[@pres-type='hard'][2]/title">
			<xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][1]/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[@pres-type='hard'][2]/title)"/> 
		</xsl:when>
		<xsl:otherwise>	<xsl:value-of select="normalize-space(ancestor::container[parent::container]/title)"/> </xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:variable name="link-video-repo" select="''"/>
<xsl:template name="getVideoBreadcrumb">
</xsl:template>

<!-- Linear Reading -->
<xsl:template match="td[(normalize-space(.) = 'Next')]">
	<td class="next">
		<xsl:apply-templates/>
	</td>
</xsl:template>
<xsl:template match="p[@indent='right']">
		<p class="align-right">
			<xsl:apply-templates />
		</p>
	</xsl:template>
		<xsl:template match="p[@indent='center']">
		<p class="align-center">
			<xsl:apply-templates />
		</p>
	</xsl:template>

</xsl:stylesheet>
